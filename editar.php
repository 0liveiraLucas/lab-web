<!doctype html>
<html lang="pt-br">
    <head>
        <title>Edição de Usuário</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-4">
                    <fieldset>
                        <legend class="text-center">Editar Usuário</legend>
                        <div class="d-flex justify-content-center">
                            <form action="editUser.php" method="post">
                                <div class="row mb-3">
                                    <label for="id" class="col-2 col-form-label">ID:</label>
                                    <div class="col-6">
                                        <input class="form-control" type="text" name="id" id="id" />
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-primary"> Buscar ID</button>
                                    </div>
                                </div>
                                <label for="nomeUser">Usuário</label>
                                <div class="mb-3">
                                    <input class="form-control" type="text" name="nomeUser" id="nomeUser" required />
                                </div>
                                <label for="senha">Nova Senha</label>
                                <div class="mb-3">
                                    <input class="form-control" type="password" name='senha' id="senha" required />
                                </div>
                                <label for="csenha">Repita a Senha</label>
                                <div class="mb-3">
                                    <input class="form-control" type="password" name="csenha" id="csenha" required />
                                </div>
                                <input class="btn btn-primary" type="submit" value="Editar" />
                            </form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </body>
</html>

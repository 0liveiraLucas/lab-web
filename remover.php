<!doctype html>
<html lang="pt-br">
    <head>
        <title>Remoção de Usuário</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-4">
                    <fieldset>
                        <legend class="text-center">Remoção de Usuário</legend>
                        <div class="d-flex justify-content-center">
                            <form action="remUser.php" method="post">
                                <div class="row mb-3">
                                    <label class="col-2 col-form-label" for="id">ID</label>
                                    <div class="col-6">
                                        <input class="form-control" type="number" name="id" id="id" required />
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-danger">Buscar</button>
                                    </div>
                                </div>
                                <input class="btn btn-danger" type="submit" value="Remover" />
                            </form>
                        </div>
                     </fieldset>
               </div>
            </div>
        </div>
    </body>
</html>

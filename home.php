<!doctype html>
<html lang="pt-br">
    <head>
        <title>Home do Sistema</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    </head>
    <body>
        <table class="table table-responsive table-striped table-hover table-bordered" border="1">
        <thead class="table-dark">
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Senha</th>
                <th>Editar</th>
                <th>Remover</th>
            </tr>
        </thead>
        <tbody>
            <?php
                require_once("controle/ControleLogin.php");
                $controle = new ControleLogin();
                $lista = $controle->selecionarTodos();
                for($i = 0; $i < sizeof($lista); $i++){
                    echo "<tr>";
                    echo "<td>{$lista[$i]->getId()}</td>";
                    echo "<td>{$lista[$i]->getNome()}</td>";
                    echo "<td>{$lista[$i]->getSenha()}</td>";
                    echo "<td><a href='editar.php?id={$lista[$i]->getId()}'>Editar</a></td>";
                    echo "<td><a href='remover.php?id={$lista[$i]->getId()}'>Remover</a></td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
        </table>
        <a class="btn btn-success" href="cadastrar.php">Novo Usuário</a>
        <a class="btn btn-primary" href="editar.php">Editar</a>
        <a class="btn btn-secondary" href="remover.php">Remover</a>
        <a class="btn btn-danger" href="sair.php">Sair</a>
    </body>
</html>

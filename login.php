<!doctype html>
<html lang='pt-br'>
    <head>
        <title>Meu sistema</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-3">
                    <fieldset>
                        <legend class="text-center">Login do usuário</legend>
                        <div class="d-flex justify-content-center">
                            <form action="procLogin.php" method="post">
                                <label for="nomeUser">Usuário</label>
                                <div class="mb-3">
                                    <input class="form-control" type="text" name="nomeUser" id="nomeUser" required />
                                </div>
                                <label for="senha">Senha</label>
                                <div class="mb-3">
                                    <input class="form-control" type="password" name="senha" id="senha" required />
                                </div>
                                <div class="mb-3">
                                    <input class="btn btn-primary" type="submit" value="Entrar" />
                                </div>
                            </form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </body>
</html>

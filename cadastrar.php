<!doctype html>
<html lang="pt-br">
    <head>
        <title>Cadastro de novo usuário</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-4">
                    <fieldset>
                        <legend class="text-center">Cadastro de Novo Usuário</legend>
                        <div class="d-flex justify-content-center">
                        <form action="cadUser.php" method="post">
                            <label for="nomeUser">Usuário</label>
                            <div class="mb-3">
                                <input class="form-control" type="text" name="nomeUser" id="nomeUser" required />
                            </div>
                            <label for="senha">Senha</label>
                            <div class="mb-3">
                                <input class="form-control" type="password" name='senha' id="senha" required />
                            </div>
                            <label for="csenha">Repita a Senha</label>
                            <div class="mb-3">
                                <input class="form-control" type="password" name="csenha" id="csenha" required />
                            </div>
                            <input class="btn btn-success" type="submit" value="Cadastrar" />
                        </form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </body>
</html>

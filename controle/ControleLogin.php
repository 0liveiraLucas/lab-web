<?php
require_once("Conexao.php");
require_once("modelo/Login.php");
class ControleLogin{
	// Tratamento de Excessões
	// try ... catch ... finally
	public function cadastrarLogin($usuario){
        try {
            $conexao = new Conexao("controle/banco.ini");
            $sql = "INSERT INTO usuarios(nome,senha) VALUES(:n, :s);";
            $comando = $conexao->getPDO()->prepare($sql);
            $nome = $usuario->getNome();
            $senha = $usuario->getSenha();
            $comando->bindParam("n",$nome);
            $comando->bindParam("s", $senha);
            if($comando->execute()){
                $retorno = true;
            }else{
                $retorno = false;
            }
        } catch (Exception $e) {
            echo("Erro encontrado: ".$e->getMessage());
        } finally {
            $conexao->fecharConexao();
            return $retorno;
        }
	}

	public function selecionarTodos(){
        try {
            $conexao = new Conexao("controle/banco.ini");
            $comando = $conexao->getPDO()->prepare("SELECT * FROM usuarios;");
            if($comando->execute()){
                $lista = $comando->fetchAll(PDO::FETCH_CLASS, "Login");
                $retorno = $lista;
            }else{
                $retorno = null;
            }
        } catch (Exception $e) {
            echo("Erro encontrado: ".$e->getMessage());
        } finally {
            $conexao->fecharConexao();
            return $retorno;
        }
	}

	public function selecionarUm($id){
        try {
            $conexao = new Conexao("controle/banco.ini");
            $comando = $conexao->getPDO()->prepare("SLECT * FROM usuarios WHERE id=:i;");
            $comando->bindParam("i", $id);
            if($comando->execute()){
                $login = $comando->fetchAll(PDO::FETCH_CLASS, "Login");
                $retorno = new Login();
                $retorno->setId($login[0]->getid());
                $retorno->setNome($login[0]->getNome());
                $retorno->setSenha($login[0]->getSenha());
            }else{
                $retorno = null;
            }
        } catch (Exception $e) {
            echo("Erro econtrado: ".$e->getMessage());
        } finally {
            $conexao->fecharConexao();
            return $retorno;
        }
	}

	public function removerLogin($id){
        try {
            $conexao = new Conexao("controle/banco.ini");
            if($conexao->getPDO()->exec("DELETE FROM usuarios WHERE id={$id};") > 0){
                $retorno = true;
            }else{
                $retorno = false;
            }
        } catch (Exception $e) {
            echo("Erro encontrado: ".$e->getMessage());
        } finally {
            $conexao->fecharConexao();
            return $retorno;
        }
	}

	public function editarLogin($id,$usuario){
        try {
            $conexao = new Conexao("controle/banco.ini");
            $comando = $conexao->getPDO()->prepare("UPDATE usuarios SET nome=:n,senha=:s WHERE id=:i");
            $nome = $usuario->getNome();
            $senha = $usuario->getSenha();
            $comando->bindParam("n", $nome);
            $comando->bindParam("s", $senha);
            $comando->bindParam("i", $id);
            if($comando->execute()){
                $retorno = true;
            }else{
                $retorno = false;
            }
        } catch (Exception $e) {
            echo("Erro encontrado: ".$e->getMessage());
        } finally {
            $conexao->fecharConexao();
            return $retorno;
        }
	}
}
?>
